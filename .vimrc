" You have installed: plug and through plug plug-ins

" General settings

colorscheme torte

set number
set relativenumber
set expandtab " only use spaces to indent
set shiftwidth=2 " indentation via >>
set tabstop=4 " how long a tab is on screen
set autoindent
set softtabstop=2 " how long a tab is from the TAB key
set hlsearch
set incsearch

if &diff
    set diffopt+=iwhite
endif

imap <C-Return> <CR><CR><C-o>k<Tab>
cnoreabbrev W w
cnoreabbrev Q q
cnoreabbrev Wq wq
cnoreabbrev WQ wq
cabbrev te tabedit
cabbrev Te tabedit

" escape from insert mode by pressing jj
imap jj <Esc>

" wrap usage of these move commands to newline
set whichwrap+=<,>,h,l,[,]

" remap ctrl-up and down to keep cursor static
nnoremap <C-Up> <C-y>
nnoremap <C-Down> <C-e>

" remap ctrl-j and k to keep cursor static
nnoremap <C-k> <C-y>
nnoremap <C-j> <C-e>

" remap ctrl-tab to tab tabedited files
nnoremap <C-Tab> :tabn<CR>
nnoremap <C-S-Tab> :tabp<CR>
