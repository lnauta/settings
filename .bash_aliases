# Commands
alias ram='watch -n 5 free -m'
alias gitlog='git log --graph --decorate --pretty=oneline --abbrev-commit --all'
alias folderTree='find . -type d | sed -e "s/[^-][^\/]*\//  |/g" -e "s/|\([^ ]\)/|-\1/"'
alias jn='jupyter notebook'
alias psa='ps -aux | grep'
alias opn='gvfs-open'
