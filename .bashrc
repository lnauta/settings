# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# added by lnauta: extends regexes
shopt -s extglob

# case insensitive completion
bind 'set completion-ignore-case on'

# recursive size
alias sizes="ls -lrShR"

# added by lnauta: spark home
export SPARK_HOME="/opt/spark/pro"
export PYTHONPATH="$SPARK_HOME/python:$SPARK_HOME/python/lib/py4j-0.10.4-src.zip:$PYTHONPATH"

# added by lnauta: pycharm link
alias pycharm="/opt/pycharm/bin/pycharm.sh"

#added by lnauta: jpp with gcc-4.4.7 and root5.32.23
cd /home/$USER/project/svn/jpp
source setenv.sh > /dev/null 
cd - > /dev/null

# added by lnauta: ROOT
#export ROOTSYS="/home/lnauta/anaconda3/envs/de"

# added by lnauta: root5 compiled
cd /usr/root5
source bin/thisroot.sh
cd - > /dev/null

# added by lnauta: root6 compiled 
#cd /usr/root6
#source bin/thisroot.sh
#cd -

# added by lnauta
export PYTHONPATH="$PYTHONPATH:/usr/local/lib/root"
export PROJECT="~/project"

# get zsh functionality in there
# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# Update bash history immediately
shopt -s histappend
PROMPT_COMMAND='history -a'

# add bash history search to arrow keyss
bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'
